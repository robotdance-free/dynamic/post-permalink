<?php
namespace rd\bd\dynamic\postpermalink;

use Breakdance\DynamicData\StringField;
use Breakdance\DynamicData\StringData;

class RD_BD_DynamicPostPermalinkField extends StringField
{

	public string $rd_bd_post_type;
	public string $rd_bd_label;
	public string $rd_bd_slug;

	public function rd_bd_set( string $slug, string $label, string $post_type ): RD_BD_DynamicPostPermalinkField
	{
		$this->rd_bd_post_type = $post_type;
		$this->rd_bd_label = $label;
		$this->rd_bd_slug = $slug;
		\Breakdance\DynamicData\registerField( $this );
		return $this;
	}

	public function label()
	{
		return $this->rd_bd_label;
	}

	public function category()
	{
		return 'Post';
	}

	public function slug()
	{
		return $this->rd_bd_slug;
	}

	public function returnTypes()
	{
		return [ 'string' ];
	}

	public function controls()
	{
		return [
			\Breakdance\Elements\control( 'rd_bd_post', 'Post', [
				'type' => 'post_chooser',
				'layout' => 'vertical',
				'postChooserOptions' => [
					'multiple' => false,
					'showThumbnails' => false,
					'postType' => $this->rd_bd_post_type
				]
			]),
			\Breakdance\Elements\control( 'rd_bd_post_params', 'Parameters', [
				'type' => 'text',
				'layout' => 'vertical',
			]),
		];
	}

	public function handler($attributes): StringData
	{
		$params = isset( $attributes[ 'rd_bd_post_params'] ) ? $attributes[ 'rd_bd_post_params' ] : '';
		$value = '';
		if ( isset( $attributes[ 'rd_bd_post' ] ) ) {
			$post = get_permalink( intval( $attributes[ 'rd_bd_post' ] ) ) ;
			$value = $post . $params . '';
		} else if ( is_singular() ) {
			$value = get_the_permalink() . $params . '';
		}
		return StringData::fromString( $value ) ;
	}

}

add_action(
	'init',
	function() {
		$post_types = get_post_types( [ 'publicly_queryable' => true, '_builtin' => true ], 'names', 'and' );
		$post_types[ 'post' ] = 'post';
		$post_types[ 'page' ] = 'page';
		foreach ( $post_types as $post_type ) {
			$class = new class extends RD_BD_DynamicPostPermalinkField {};
			$class->rd_bd_set( 'rd_bd_dynamic_post_permalink_' . $post_type, 'Post Permalink ( '. $post_type .' )', $post_type );
		}
	}
);
